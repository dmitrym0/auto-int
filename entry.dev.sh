#!/bin/bash
set -x
cd /home/app

bundle install --binstubs=/bundle-cache/bin --path=/bundle-cache

bundle exec bin/rails s -b 0.0.0.0
