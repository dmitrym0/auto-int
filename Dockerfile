FROM phusion/passenger-ruby26:1.0.9

ENV APP_HOME /home/app

RUN apt-get update && \
    apt-get install -y \
    tzdata netcat

# slight caching optimization: if these files don't change then docker uses previously cached fs layers
# and doesn't reinstall everything from scratch.
COPY Gemfile $APP_HOME
COPY Gemfile.lock $APP_HOME

WORKDIR $APP_HOME

# install required gems, making sure that the destination is under /home/app
RUN bundle install --binstubs=/home/app/.local/bin --path=/home/app/.local/bundler

# atb-env.conf contans environment variables that NGINX will pass from host to the ruby processes.
COPY auto-int-env.conf /etc/nginx/main.d/auto-int-env.conf

# required by the parent image
RUN rm -f /etc/service/nginx/down
RUN rm /etc/nginx/sites-enabled/default
COPY webapp.conf /etc/nginx/sites-enabled/
COPY app_startup.sh /etc/my_init.d/90_app_startup.sh

# copy docker 'context' into /home/app
COPY . $APP_HOME

# ensure dir ownership is correct
RUN chown --recursive app:app /home/app

# precompile assets
# not necessary here.
# RUN su app -c 'RAILS_ENV=production bundle exec rails assets:precompile'

