#!/bin/bash

cd /home/app/
export RAILS_ENV=production

echo "Ensure database is up before starting ..."
while ( ! ( nc -w 1 -z db 5432 ) )  && [ -z $DATABASE_URL ]; do sleep 1; echo 'Waiting for the db to become available...'; done
echo "Database is up..."

echo "[Optional] Running migrations..."

echo "[Optional] ...create db..."
/sbin/setuser app bundle exec rake db:create 2>&1

echo "[Optional] ...migrate db..."
/sbin/setuser app bundle exec rake db:migrate 2>&1

echo "Migrations done."
