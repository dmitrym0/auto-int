
# Table of Contents

1.  [Readme](#orgf81de20)
    1.  [Date](#orga8ec6eb)
    2.  [Location of deployed application](#org157354b)
    3.  [Time spent](#org64cc107)
    4.  [Assumptions made](#org7e0b686)
    5.  [Shortcuts/Compromises made](#org69fa207)
    6.  [Stretch goals attempted](#org89f5515)
    7.  [Instructions to run assignment locally](#org2d97ea1)
    8.  [What did you not include in your solution that you want us to know about?](#orgbf2dadf)
    9.  [Introduction](#orgc760426)
    10. [Production Image](#org4b14869)
    11. [Development image](#orgf286685)
    12. [SSL Support](#org206bc99)
        1.  [Development](#org3d2c75d)
        2.  [Production](#org3e4c9a0)
    13. [Logging](#org08bc512)
    14. [Architecture and Roll out plan](#org2c52095)
        1.  [Architecture diagram](#org94b5350)
    15. [Multitenancy](#orge291d23)
        1.  [Multitenancy at the application level](#org004a87b)
        2.  [Multitenancy at the operational level](#org26f421e)
        3.  [Building and Deploying](#org08764c2)
        4.  [Production](#org2c285d8)
        5.  [Local option](#org4519de9)


<a id="orgf81de20"></a>

# Readme


<a id="orga8ec6eb"></a>

## Date

January 19, 2020


<a id="org157354b"></a>

## Location of deployed application

<https://auto-int.duckdns.org>


<a id="org64cc107"></a>

## Time spent

Approximately 5 hours.


<a id="org7e0b686"></a>

## Assumptions made

-   prod configuration assumes that there's a single node.
-   assumption that the app is API only (no asset precompilation).


<a id="org69fa207"></a>

## Shortcuts/Compromises made

-   no build or roll out procedure, everything is manual


<a id="org89f5515"></a>

## Stretch goals attempted

1.  logging and live deployment went well.
2.  roll out and build procedure need to improve (containers need to be versioned)


<a id="org2d97ea1"></a>

## Instructions to run assignment locally

See below.


<a id="orgbf2dadf"></a>

## What did you not include in your solution that you want us to know about?

Basically anything that requires manual interaction needs to be automated. For this assignment it was primarily building and deployment.


<a id="orgc760426"></a>

## Introduction

Per the assignment, my goal was to come up with two containers:

-   production grade container for autoinrementing-integers (auto-int from here on).
-   development image (to ease the development process).


<a id="org4b14869"></a>

## Production Image

I opted to use Phusion base image, because I've used it in the past with good success. I knew that if I sucessufully manage to create a production image, it would be trivial to create a "development" image from that.

Phusion is the company behind Passenger, a Rails application server. They have a pretty [good explanation](http://phusion.github.io/baseimage-docker/) as to why a complete base image should be used (briefly, correct startup, process management and periodic jobs).
Phusion passenger runs under nginx and manages the underlying ruby processes. To fully take advantage of Passenger I had to add the following:

-   `Dockefile` that defines how the container is built
-   `webapp.conf` that defines where in the container the application lives
-   `auto-int-env.conf` that specifies what environment variables will be passed to rails applications
-   `app_startup.sh` that deals with setting up database environment and any house keeping when the container comes up.
-   `docker-compose.yml` and `docker-compose.prod` specify how to start the environment in production (more on it later)


<a id="orgf286685"></a>

## Development image

There's is no development image per se. Since the production image contains all of the necessary resources already (Ruby, gems, etc) we can use  `docker-compose.dev.yml` (in addition to `docker-compose.yml`) to start the image
in "development mode". To achieve that we use the production image but instead of starting nginx-passenger combination we invoke `rails s` directly and mount the source from users local directory. This allows developers to develop in
near production like environment.  `entry.dev.sh` contains the specifics.


<a id="org206bc99"></a>

## SSL Support


<a id="org3d2c75d"></a>

### Development

In development, the easiest way to achieve SSL is to install [local-ssl-proxy](https://github.com/jqk6/local-ssl-proxy). It automates away the need to create SSL certificates:

    > npm install local-ssl-proxy
    ...
    > local-ssl-proxy --source 4000 --target 3000
    Started proxy: https://localhost:4000 → http://localhost:3000
    > docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
    ...
     Puma starting in single mode...
     * Version 3.12.1 (ruby 2.6.5-p114), codename: Llamas in Pajamas
     * Min threads: 5, max threads: 5
     * Environment: development
     * Listening on tcp://0.0.0.0:3000

Now all requests to `https://localhost:4000` will be proxied to port 3000.


<a id="org3e4c9a0"></a>

### Production

For production SSL support, the story is arguably simpler &#x2013; at least in the single node scenario. 

[Lets Encrypt nginx Proxy companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion) takes care of everything for us. It's a standalone container that monitors local docker events and when a new container comes up that has `VIRTUAL_HOST` environment variables set, 
it requests SSL certificates from Lets Encrypt and updated the `nginx-proxy` continaer with their SSL configuration. This is really convenient for single instance machines.

For a load balanced, multi node setup the configuration would be different. SSL would likely terminate at the load balancer.


<a id="org08bc512"></a>

## Logging

Out of the options provided I decided to implement Logging. I'm using the same approach here that I used for SSL: composition of specialized containers. For capturing rails logging it's possible to use a specialized gem.
However Rails already includes a provision to log to output (via the `RAILS_LOG_TO_STDOUT` environment variable). Since all processes are logging to `STDOUT`, it's now trivial to capture that output and redirect it elsehwere.

This can be achieved with the [logspout](https://github.com/gliderlabs/logspout) container. It's then possible to configure logspout to redirect logs to greylog, logstash or any of the other major log sinks.

The advantage of this approach is that it makes logging an operational concern rather than a development task. Configuration now lives with deployment rather than code.

One note though, is that Rails application logs need to be massaged to fit within this paradigm. Currently Rails logs are multiline they need to be compressed to be a single line with one of the available gems.


<a id="org2c52095"></a>

## Architecture and Roll out plan

Since we're using `docker-compose` for production roll out to a single node, the roll out plan is quite simple:

1.  Provision a node (VM)
2.  Install docker
3.  Copy `docker-compose.yml` and `docker-compose.prod.yml`
4.  Run `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`


<a id="org94b5350"></a>

### Architecture diagram

![img](images/Readme/screenshot_2020-01-19_11-09-53.png)

The architecture is quite straightforward.

-   auto<sub>int</sub><sub>web</sub><sub>app</sub> is a container housing the application
-   postgres<sub>db</sub> is a container with the database
-   nginx<sub>proxy</sub> handles external web requests and marshals them appropriately (in this case directly to auto<sub>int</sub><sub>web</sub><sub>app</sub>)
-   LetsencryptCompoanion takes care of requesting and updating SSL certificates
-   Logspout ingests all logs events from various containers
-   USER is the user.


<a id="orge291d23"></a>

## Multitenancy

There are a couple of ways I can see multitenancy implemented. A lot depends on the performance profile as well as application design. 


<a id="org004a87b"></a>

### Multitenancy at the application level

Compartmentalize access at the business objects level. All objects live within a single database/system and access is determined based on business logic. The downside is inability to partition access & performance.

If a single user on a multitenant system monopolizes all of the resources the rest of the system will suffer. The upside is operational simplicity &#x2013; to a certain extent. At some point adding more resources won't improve performance at which point you'd have to move to plan "B".


<a id="org26f421e"></a>

### Multitenancy at the operational level

In this case, we can either have a collection of single tenant applications, or a collection of miltitenant applications (as described in the previous paragraph). Rather than having a set of tenants living on one cluster, we invoke some business logic to determine where teants could live:

Collection of single tenants:

-   ClusterA: Tenant A
-   ClusterB: Tenant B

&#x2026;

-   ClusterZ: Tenant Z

Collection of multitenants:

-   Cluster A: Tenant 1,2,3
-   Cluster B: Tenant 4,5,6

&#x2026;

-   Cluster Z: Tenant 98,99,100

Again, a lot will depend on the application specifics. In the case of "single tenant per cluster", we may end up wasting resources. A "collection of multitenants" has some nice properties. For example if  client 2 is hogging all of the resources, we may impact clients 1 and 2, but the other clusters will not be impacted.

Of course all of this comes at the expense of operational complexity. So the system has to be careful designed based on the existing performance metrics and current requirements.


<a id="org08764c2"></a>

### Building and Deploying

1.  On the build machine

    1.  Build production container: `docker build  -t dmarkush/auto-int .`
    2.  Push container: `docker push dmarkush/auto-int`
    3.  Copy production runtime config: `scp root@auto-int.duckdns.org:~/auto-int/docker*.yml .`

2.  On remote machine

    1.  `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`
    2.  Verify that SSL is enabled (get a 301 to HTTPS):
    
```
> curl -v --data '{"username": "dmitrym@gmail.com", "password": "123456"}' --header "Content-Type:application/json" --request POST http://auto-int.duckdns.org/users/create
 HTTP/1.1 301 Moved Permanently
< Server: nginx/1.13.5
< Date: Sun, 19 Jan 2020 22:29:19 GMT
< Content-Type: text/html
< Content-Length: 185
< Connection: keep-alive
< Location: https://auto-int.duckdns.org/users/create
<
<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
```    
1.  Go through a test flow
    
```     
❯ curl --data '{"username": "dmitrym1@gmail.com", "password": "123456"}' --header "Content-Type:application/json" --request POST https://auto-int.duckdns.org/users/create
{"data":{"id":"2","type":"user","attributes":{"api_key":"irbcYyqHy6X74FuTJrX1Vt1r"}},"jsonapi":{"version":"1.0"}}%
❯ curl --header "Authorization: Bearer irbcYyqHy6X74FuTJrX1Vt1r" --request PUT https://auto-int.duckdns.org/api/v1/integer/increment                                                                   
{"data":{"id":"2","type":"user_integer","attributes":{"value":2}},"jsonapi":{"version":"1.0"}}% 
```

<a id="org2c285d8"></a>

### Production

"Production" version of the app is deployed to <https://auto-int.duckdns.org>. I've attached a private key (`thinkific`) that you can use to inspect the running instance:

    ssh -ithinkific root@auto-int.duckdns.org


<a id="org4519de9"></a>

### Local option

To run a local production instance:

-   `docker-compose -f docker-compose.yml -f docker-compose.local.yml up`

